var test = require('tape');
var usage = require('cpu-percentage');
var fs = require('fs');
var TooHot = require('./index');

test('returns false when cold', (t) => {
  var tooHot = TooHot({ceiling: 100});
  t.equal(tooHot(), false);
  t.end();
});

test('returns promise when hot', (t) => {
  var tooHot = TooHot({ceiling: 1});
  for (let x of Array(3e3)) fs.readFileSync('./package.json');
  t.notEqual(tooHot(), false);
  t.equal(typeof tooHot().then, 'function');
  t.end();
});

test('respects `maxPause` opt', async (t) => {
  t.plan(1);
  t.timeoutAfter(8000);

  const ceiling = 0.01; // super low threshold, most likely never met
  const maxPause = 50; // ms
  const tooHot = TooHot({ceiling, maxPause, wait: 16});

  for (let x of Array(200)) {
    const contents = fs.readFileSync('./package.json');
    Buffer.from(contents.toString('hex').toUpperCase())
      .toString('base64')
      .toLowerCase()
      .substr(0, 20);
    let hot;
    if ((hot = tooHot())) await hot;
  }
  t.pass('all data points processed');
  t.end();
});

test('heavy processing is below the prescribed CPU 90% ceiling', async (t) => {
  t.plan(1);
  t.timeoutAfter(20e3);

  const ceiling = 90;
  const tooHot = TooHot({ceiling});
  const start = usage();

  for (let x of Array(20000)) {
    const contents = fs.readFileSync('./package.json');
    Buffer.from(contents.toString('hex').toUpperCase())
      .toString('base64')
      .toLowerCase()
      .substr(0, 20);
    let hot;
    if ((hot = tooHot())) await hot;
  }
  const stats = usage(start);
  console.log(usage(start));
  t.true(stats.percent < ceiling + 3, 'CPU usage was approx. 90%');
});

test('heavy processing is below the prescribed CPU 50% ceiling', async (t) => {
  t.plan(1);
  t.timeoutAfter(20e3);

  const ceiling = 50;

  const tooHot = TooHot({ceiling});
  const start = usage();

  for (let x of Array(8000)) {
    const contents = fs.readFileSync('./package.json');
    Buffer.from(contents.toString('hex').toUpperCase())
      .toString('base64')
      .toLowerCase()
      .substr(0, 20);
    let hot;
    if ((hot = tooHot())) await hot;
  }
  const stats = usage(start);
  console.log(usage(start));
  t.true(stats.percent < ceiling + 3, 'CPU usage was approx. 50%');
});
