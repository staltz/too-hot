var usage = require('cpu-percentage');

module.exports = function TooHot(opts) {
  var _opts = opts || {ceiling: 88, wait: 144, maxPause: Infinity};
  var ceiling = _opts.ceiling || 88;
  var wait = _opts.wait || 144;
  var maxPause = _opts.maxPause || Infinity;
  var start = (stats = usage());
  var lastResume = Date.now();
  var step = 2,
    i = 0;

  return function tooHot() {
    if (i++ % step) return false;

    stats = usage(start);
    if (
      stats.percent < ceiling || // "CPU is cold enough"
      Date.now() - lastResume > maxPause || // "remained paused for too long"
      stats.time < 20 // "we just now began draining"
    ) {
      step = step << 1 || 1;
      lastResume = Date.now();
      return false;
    } else {
      step = step >> 1 || 1;
      return new Promise((resolve) => {
        setTimeout(resolve, wait);
      });
    }
  };
};
