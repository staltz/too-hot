# too-hot

> Answers whether your CPU is too busy

```bash
npm install --save too-hot
```

Similar to [pull-drain-gently](https://gitlab.com/staltz/pull-drain-gently), but for general purpose.

## Usage

### Default use

```js
const TooHot = require('too-hot');

async function main() {
  const tooHot = TooHot()
  for (let i = 0; i < 10000; i++) {
    someKindOfHeavyProcessing()
    const hot = tooHot() // returns a Promise or `false`
    if (hot) await hot
  }
}
```

For `too-hot` to be effective and to make sense, you should only use this when iterating over a large chunk of data. Otherwise, the timers inside tooHot() will be too spaced apart and the CPU stats will lose precision.

### Tweaking the parameters

Every once in a while, `tooHot()` will check the CPU usage, and if it has gone above the limit known as the `ceiling`, it returns a promise that (when `.then`d or `await`ened) will wait for `wait` milliseconds, in such a way that to keep CPU a bit under the chosen `ceiling` limit. In other words, two parameters control the behavior:

- `ceiling`: the maximum CPU usage you want the Node.js process to consume, approximately, in percentages (`100` is 100%, not `1`)
- `wait`: the waiting period, in milliseconds, when pausing in order to allow other tasks to use the CPU

The default CPU ceiling is `88%` and the default waiting period for each pause is `144ms` (roughly 9 frames if you have the UI running at 60fps). If the CPU remains hot even after the returned promise resolves, the next `tooHot()` will also return a promise to create another pause. This can keep on going indefinitely if the CPU remains hot.

There is also a third less common parameter, which by default is turned off:

- `maxPause`: a limit in milliseconds for how long to pause (caused by consecutive promises returned). For instance, when `maxPause = 5000`, if the last time `tooHot()` returned `false` was 5 seconds ago, then it will necessarily return `false` this time, _regardless_ of the current CPU usage. **The default is `Infinity`**.

To configure your own parameters, pass an `opts` object as the argument to `gently`:

```js
const TooHot = require('too-hot');

async function main() {
  const tooHot = TooHot({ ceiling: 90, wait: 60 })
  for (let i = 0; i < 10000; i++) {
    someKindOfHeavyProcessing()
    const hot = tooHot()
    if (hot) await hot
  }
}
```

To configure these parameters, consider that:

- The **greater the `ceiling`** is, the closer your program behaves as if `too-hot` were not used, i.e. the **more hot** your CPU will run on heavy processing
- The **smaller the `ceiling`** is, the **more time** it will take to complete the processing of data, i.e. the slower your application will finish its CPU tasks
- The **greater the `wait`**, the **more fluctuation** of CPU usage will happen, both below and above the `ceiling`, i.e. the more bumpy the ride is for CPU usage and workload throughput
- The **smaller the `wait`**, the more the actual CPU usage accurately meets the `ceiling`, but also the **more overhead** there is with many short-lived timers for those pauses

The total time for completing processing is important. Not using `too-hot` is the *fastest* in total time, but using `too-hot` with a small `wait` might give a total  time of approx. 2.5x slower than the fastest.

The defaults `ceiling=88`, `wait=144` are a sweet spot, and it can achieve a total time of approx 1.4x slower than the fastest.

The table below shows how the JS CPU flamechart looks like when running the benchmarks on Ubuntu 18.04.3 x86_64, Intel® Core™ i7-7500U CPU @ 2.70GHz × 4, 15,4 GiB RAM, for different values of the `ceiling` parameter. "Unlimited" means "`too-hot` was not used":

| CPU `ceiling` | CPU Profiler in Chrome |
|-----------|------------------------|
| Unlimited | ![100](./images/100.png) |
| `90`% | ![90](./images/90.png) |
| `80`% | ![80](./images/80.png) |
| `70`% | ![70](./images/70.png) |
| `60`% | ![60](./images/60.png) |
| `50`% | ![50](./images/50.png) |
| `40`% | ![40](./images/40.png) |

## License

[MIT](https://tldrlegal.com/license/mit-license)
